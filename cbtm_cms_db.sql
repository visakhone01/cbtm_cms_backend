/*
SQLyog Ultimate
MySQL - 11.0.2-MariaDB : Database - cbtm_cms_db
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
/*Table structure for table `auth_menu` */

CREATE TABLE `auth_menu` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `menu_code` varchar(255) DEFAULT NULL,
  `menu_name` varchar(255) DEFAULT NULL,
  `menu_url` varchar(255) DEFAULT NULL,
  `menu_icon` varchar(255) DEFAULT NULL,
  `menu_image` varchar(255) DEFAULT NULL,
  `menu_parent` int(11) DEFAULT NULL,
  `menu_level` int(11) DEFAULT NULL,
  `menu_group` int(11) DEFAULT NULL,
  `menu_order` int(11) DEFAULT NULL,
  `menu_status` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_general_ci;

/*Data for the table `auth_menu` */

insert  into `auth_menu`(`id`,`menu_code`,`menu_name`,`menu_url`,`menu_icon`,`menu_image`,`menu_parent`,`menu_level`,`menu_group`,`menu_order`,`menu_status`,`created_at`,`updated_at`,`deleted_at`) values (1,NULL,'ໜ້າທຳອິດ','index','mdi mdi-laptop',NULL,1,0,1,1,'Y',NULL,NULL,NULL);
insert  into `auth_menu`(`id`,`menu_code`,`menu_name`,`menu_url`,`menu_icon`,`menu_image`,`menu_parent`,`menu_level`,`menu_group`,`menu_order`,`menu_status`,`created_at`,`updated_at`,`deleted_at`) values (2,NULL,'ເອກະສານ CBTM',NULL,'mdi mdi-file',NULL,2,1,2,2,'N',NULL,NULL,NULL);
insert  into `auth_menu`(`id`,`menu_code`,`menu_name`,`menu_url`,`menu_icon`,`menu_image`,`menu_parent`,`menu_level`,`menu_group`,`menu_order`,`menu_status`,`created_at`,`updated_at`,`deleted_at`) values (3,NULL,'ພີມໃບປ່ອຍລວມ','test2','mdi mdi-printer',NULL,2,2,2,20001,'N',NULL,NULL,NULL);
insert  into `auth_menu`(`id`,`menu_code`,`menu_name`,`menu_url`,`menu_icon`,`menu_image`,`menu_parent`,`menu_level`,`menu_group`,`menu_order`,`menu_status`,`created_at`,`updated_at`,`deleted_at`) values (4,NULL,'ລາຍການພີມໃບປ່ອຍລວມ','document-release','mdi mdi-text-box-multiple-outline',NULL,2,1,2,2,'Y',NULL,NULL,NULL);
insert  into `auth_menu`(`id`,`menu_code`,`menu_name`,`menu_url`,`menu_icon`,`menu_image`,`menu_parent`,`menu_level`,`menu_group`,`menu_order`,`menu_status`,`created_at`,`updated_at`,`deleted_at`) values (5,NULL,'ການເງີນ',NULL,'mdi mdi-finance',NULL,5,1,5,5,'Y',NULL,NULL,NULL);
insert  into `auth_menu`(`id`,`menu_code`,`menu_name`,`menu_url`,`menu_icon`,`menu_image`,`menu_parent`,`menu_level`,`menu_group`,`menu_order`,`menu_status`,`created_at`,`updated_at`,`deleted_at`) values (6,NULL,'ຮັບເງີນເອກະສານຂົນສົ່ງ','finance-add','mdi mdi-cash-multiple',NULL,5,2,5,50001,'Y',NULL,NULL,NULL);
insert  into `auth_menu`(`id`,`menu_code`,`menu_name`,`menu_url`,`menu_icon`,`menu_image`,`menu_parent`,`menu_level`,`menu_group`,`menu_order`,`menu_status`,`created_at`,`updated_at`,`deleted_at`) values (7,NULL,'ລາຍການຮັບເງີນເອກະສານຂົນສົ່ງ','finance-list','mdi mdi-clipboard-list-outline',NULL,5,2,5,50002,'Y',NULL,NULL,NULL);

/*Table structure for table `auth_permision` */

CREATE TABLE `auth_permision` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `permision_code` varchar(255) DEFAULT NULL,
  `role_id` int(11) DEFAULT NULL,
  `permision_status` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_general_ci;

/*Data for the table `auth_permision` */

/*Table structure for table `auth_permision_role` */

CREATE TABLE `auth_permision_role` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `permision_role_code` varchar(255) DEFAULT NULL,
  `permision_role_name` varchar(255) DEFAULT NULL,
  `permision_role_desc` varchar(255) DEFAULT NULL,
  `permision_role_status` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_general_ci;

/*Data for the table `auth_permision_role` */

/*Table structure for table `auth_role` */

CREATE TABLE `auth_role` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `role_name` varchar(255) DEFAULT NULL,
  `role_desc` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_general_ci;

/*Data for the table `auth_role` */

insert  into `auth_role`(`id`,`role_name`,`role_desc`,`created_at`,`updated_at`,`deleted_at`) values (1,'Supper Admin',NULL,NULL,NULL,NULL);

/*Table structure for table `auth_role_users` */

CREATE TABLE `auth_role_users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `role_id` int(11) DEFAULT NULL,
  `role_users_status` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_general_ci;

/*Data for the table `auth_role_users` */

insert  into `auth_role_users`(`id`,`user_id`,`role_id`,`role_users_status`,`created_at`,`updated_at`,`deleted_at`) values (1,1,1,'Y',NULL,NULL,NULL);

/*Table structure for table `cache` */

CREATE TABLE `cache` (
  `key` varchar(255) NOT NULL,
  `value` mediumtext NOT NULL,
  `expiration` int(11) NOT NULL,
  PRIMARY KEY (`key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_general_ci;

/*Data for the table `cache` */

/*Table structure for table `cache_locks` */

CREATE TABLE `cache_locks` (
  `key` varchar(255) NOT NULL,
  `owner` varchar(255) NOT NULL,
  `expiration` int(11) NOT NULL,
  PRIMARY KEY (`key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_general_ci;

/*Data for the table `cache_locks` */

/*Table structure for table `document_release` */

CREATE TABLE `document_release` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `docr_group_code` varchar(255) DEFAULT NULL,
  `docr_group_rno` decimal(18,0) DEFAULT NULL,
  `docr_code` varchar(255) DEFAULT NULL,
  `docr_date` date DEFAULT NULL,
  `province_id` varchar(255) DEFAULT NULL,
  `border_id` varchar(255) DEFAULT NULL,
  `company_id` varchar(255) DEFAULT NULL,
  `company_name` varchar(255) DEFAULT NULL,
  `mbl_no` varchar(255) DEFAULT NULL,
  `declaration_type` varchar(255) DEFAULT NULL,
  `icm_no` varchar(255) DEFAULT NULL,
  `icm_date` date DEFAULT NULL,
  `plate_number` varchar(255) DEFAULT NULL,
  `release_status` varchar(255) DEFAULT NULL,
  `remark` text DEFAULT NULL,
  `docr_group` decimal(18,0) DEFAULT NULL,
  `docr_status` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_general_ci;

/*Data for the table `document_release` */

insert  into `document_release`(`id`,`docr_group_code`,`docr_group_rno`,`docr_code`,`docr_date`,`province_id`,`border_id`,`company_id`,`company_name`,`mbl_no`,`declaration_type`,`icm_no`,`icm_date`,`plate_number`,`release_status`,`remark`,`docr_group`,`docr_status`,`created_at`,`updated_at`,`deleted_at`) values (1,'202403000001',1,'20240330001','2024-03-30','province_id','border_id','company_id','company_name','MBL000001','declaration_type','icm_no','2024-03-30','plate_number','release_status','remark',1,'Y','2024-03-30 23:41:11','2024-03-30 23:41:11',NULL);
insert  into `document_release`(`id`,`docr_group_code`,`docr_group_rno`,`docr_code`,`docr_date`,`province_id`,`border_id`,`company_id`,`company_name`,`mbl_no`,`declaration_type`,`icm_no`,`icm_date`,`plate_number`,`release_status`,`remark`,`docr_group`,`docr_status`,`created_at`,`updated_at`,`deleted_at`) values (2,'202403000001',1,'20240330002','2024-03-30','province_id','border_id','company_id','company_name','MBL000002','declaration_type','icm_no','2024-03-30','plate_number','release_status','remark',1,'Y','2024-03-30 23:41:11','2024-03-30 23:41:11',NULL);
insert  into `document_release`(`id`,`docr_group_code`,`docr_group_rno`,`docr_code`,`docr_date`,`province_id`,`border_id`,`company_id`,`company_name`,`mbl_no`,`declaration_type`,`icm_no`,`icm_date`,`plate_number`,`release_status`,`remark`,`docr_group`,`docr_status`,`created_at`,`updated_at`,`deleted_at`) values (3,'202403000002',2,'20240330003','2024-03-30','province_id','border_id','company_id','company_name','MBL000003','declaration_type','icm_no','2024-03-30','plate_number','release_status','remark',2,'Y','2024-03-30 23:41:11','2024-03-30 23:41:11',NULL);
insert  into `document_release`(`id`,`docr_group_code`,`docr_group_rno`,`docr_code`,`docr_date`,`province_id`,`border_id`,`company_id`,`company_name`,`mbl_no`,`declaration_type`,`icm_no`,`icm_date`,`plate_number`,`release_status`,`remark`,`docr_group`,`docr_status`,`created_at`,`updated_at`,`deleted_at`) values (4,'202403000002',2,'20240330004','2024-03-30','province_id','border_id','company_id','company_name','MBL000004','declaration_type','icm_no','2024-03-30','plate_number','release_status','remark',2,'Y','2024-03-30 23:41:11','2024-03-30 23:41:11',NULL);
insert  into `document_release`(`id`,`docr_group_code`,`docr_group_rno`,`docr_code`,`docr_date`,`province_id`,`border_id`,`company_id`,`company_name`,`mbl_no`,`declaration_type`,`icm_no`,`icm_date`,`plate_number`,`release_status`,`remark`,`docr_group`,`docr_status`,`created_at`,`updated_at`,`deleted_at`) values (5,'202403000002',2,'20240330005','2024-03-30','province_id','border_id','company_id','company_name','MBL000005','declaration_type','icm_no','2024-03-30','plate_number','release_status','remark',2,'Y','2024-03-30 23:41:11','2024-03-30 23:41:11',NULL);
insert  into `document_release`(`id`,`docr_group_code`,`docr_group_rno`,`docr_code`,`docr_date`,`province_id`,`border_id`,`company_id`,`company_name`,`mbl_no`,`declaration_type`,`icm_no`,`icm_date`,`plate_number`,`release_status`,`remark`,`docr_group`,`docr_status`,`created_at`,`updated_at`,`deleted_at`) values (6,'202403000003',3,'20240330006','2024-03-30','province_id','border_id','company_id','company_name','MBL000001','declaration_type','icm_no','2024-03-30','plate_number','release_status','remark',3,'Y','2024-03-30 23:41:12','2024-03-30 23:41:12',NULL);
insert  into `document_release`(`id`,`docr_group_code`,`docr_group_rno`,`docr_code`,`docr_date`,`province_id`,`border_id`,`company_id`,`company_name`,`mbl_no`,`declaration_type`,`icm_no`,`icm_date`,`plate_number`,`release_status`,`remark`,`docr_group`,`docr_status`,`created_at`,`updated_at`,`deleted_at`) values (7,'202403000003',3,'20240330007','2024-03-30','province_id','border_id','company_id','company_name','MBL000002','declaration_type','icm_no','2024-03-30','plate_number','release_status','remark',3,'Y','2024-03-30 23:41:12','2024-03-30 23:41:12',NULL);
insert  into `document_release`(`id`,`docr_group_code`,`docr_group_rno`,`docr_code`,`docr_date`,`province_id`,`border_id`,`company_id`,`company_name`,`mbl_no`,`declaration_type`,`icm_no`,`icm_date`,`plate_number`,`release_status`,`remark`,`docr_group`,`docr_status`,`created_at`,`updated_at`,`deleted_at`) values (8,'202403000004',4,'20240330008','2024-03-30','province_id','border_id','company_id','company_name','MBL000003','declaration_type','icm_no','2024-03-30','plate_number','release_status','remark',4,'Y','2024-03-30 23:41:12','2024-03-30 23:41:12',NULL);
insert  into `document_release`(`id`,`docr_group_code`,`docr_group_rno`,`docr_code`,`docr_date`,`province_id`,`border_id`,`company_id`,`company_name`,`mbl_no`,`declaration_type`,`icm_no`,`icm_date`,`plate_number`,`release_status`,`remark`,`docr_group`,`docr_status`,`created_at`,`updated_at`,`deleted_at`) values (9,'202403000004',4,'20240330009','2024-03-30','province_id','border_id','company_id','company_name','MBL000004','declaration_type','icm_no','2024-03-30','plate_number','release_status','remark',4,'Y','2024-03-30 23:41:12','2024-03-30 23:41:12',NULL);
insert  into `document_release`(`id`,`docr_group_code`,`docr_group_rno`,`docr_code`,`docr_date`,`province_id`,`border_id`,`company_id`,`company_name`,`mbl_no`,`declaration_type`,`icm_no`,`icm_date`,`plate_number`,`release_status`,`remark`,`docr_group`,`docr_status`,`created_at`,`updated_at`,`deleted_at`) values (10,'202403000004',4,'20240330010','2024-03-30','province_id','border_id','company_id','company_name','MBL000005','declaration_type','icm_no','2024-03-30','plate_number','release_status','remark',4,'Y','2024-03-30 23:41:12','2024-03-30 23:41:12',NULL);
insert  into `document_release`(`id`,`docr_group_code`,`docr_group_rno`,`docr_code`,`docr_date`,`province_id`,`border_id`,`company_id`,`company_name`,`mbl_no`,`declaration_type`,`icm_no`,`icm_date`,`plate_number`,`release_status`,`remark`,`docr_group`,`docr_status`,`created_at`,`updated_at`,`deleted_at`) values (11,'202403000005',5,'20240330011','2024-03-30','province_id','border_id','company_id','company_name','MBL000001','declaration_type','icm_no','2024-03-30','plate_number','release_status','remark',5,'Y','2024-03-30 23:41:12','2024-03-30 23:41:12',NULL);
insert  into `document_release`(`id`,`docr_group_code`,`docr_group_rno`,`docr_code`,`docr_date`,`province_id`,`border_id`,`company_id`,`company_name`,`mbl_no`,`declaration_type`,`icm_no`,`icm_date`,`plate_number`,`release_status`,`remark`,`docr_group`,`docr_status`,`created_at`,`updated_at`,`deleted_at`) values (12,'202403000005',5,'20240330012','2024-03-30','province_id','border_id','company_id','company_name','MBL000002','declaration_type','icm_no','2024-03-30','plate_number','release_status','remark',5,'Y','2024-03-30 23:41:12','2024-03-30 23:41:12',NULL);
insert  into `document_release`(`id`,`docr_group_code`,`docr_group_rno`,`docr_code`,`docr_date`,`province_id`,`border_id`,`company_id`,`company_name`,`mbl_no`,`declaration_type`,`icm_no`,`icm_date`,`plate_number`,`release_status`,`remark`,`docr_group`,`docr_status`,`created_at`,`updated_at`,`deleted_at`) values (13,'202403000006',6,'20240330013','2024-03-30','province_id','border_id','company_id','company_name','MBL000003','declaration_type','icm_no','2024-03-30','plate_number','release_status','remark',6,'Y','2024-03-30 23:41:12','2024-03-30 23:41:12',NULL);
insert  into `document_release`(`id`,`docr_group_code`,`docr_group_rno`,`docr_code`,`docr_date`,`province_id`,`border_id`,`company_id`,`company_name`,`mbl_no`,`declaration_type`,`icm_no`,`icm_date`,`plate_number`,`release_status`,`remark`,`docr_group`,`docr_status`,`created_at`,`updated_at`,`deleted_at`) values (14,'202403000006',6,'20240330014','2024-03-30','province_id','border_id','company_id','company_name','MBL000004','declaration_type','icm_no','2024-03-30','plate_number','release_status','remark',6,'Y','2024-03-30 23:41:12','2024-03-30 23:41:12',NULL);
insert  into `document_release`(`id`,`docr_group_code`,`docr_group_rno`,`docr_code`,`docr_date`,`province_id`,`border_id`,`company_id`,`company_name`,`mbl_no`,`declaration_type`,`icm_no`,`icm_date`,`plate_number`,`release_status`,`remark`,`docr_group`,`docr_status`,`created_at`,`updated_at`,`deleted_at`) values (15,'202403000006',6,'20240330015','2024-03-30','province_id','border_id','company_id','company_name','MBL000005','declaration_type','icm_no','2024-03-30','plate_number','release_status','remark',6,'Y','2024-03-30 23:41:12','2024-03-30 23:41:12',NULL);
insert  into `document_release`(`id`,`docr_group_code`,`docr_group_rno`,`docr_code`,`docr_date`,`province_id`,`border_id`,`company_id`,`company_name`,`mbl_no`,`declaration_type`,`icm_no`,`icm_date`,`plate_number`,`release_status`,`remark`,`docr_group`,`docr_status`,`created_at`,`updated_at`,`deleted_at`) values (16,'202403000007',7,'20240330016','2024-03-30','province_id','border_id','company_id','company_name','MBL000001','declaration_type','icm_no','2024-03-30','plate_number','release_status','remark',7,'Y','2024-03-30 23:41:13','2024-03-30 23:41:13',NULL);
insert  into `document_release`(`id`,`docr_group_code`,`docr_group_rno`,`docr_code`,`docr_date`,`province_id`,`border_id`,`company_id`,`company_name`,`mbl_no`,`declaration_type`,`icm_no`,`icm_date`,`plate_number`,`release_status`,`remark`,`docr_group`,`docr_status`,`created_at`,`updated_at`,`deleted_at`) values (17,'202403000007',7,'20240330017','2024-03-30','province_id','border_id','company_id','company_name','MBL000002','declaration_type','icm_no','2024-03-30','plate_number','release_status','remark',7,'Y','2024-03-30 23:41:13','2024-03-30 23:41:13',NULL);
insert  into `document_release`(`id`,`docr_group_code`,`docr_group_rno`,`docr_code`,`docr_date`,`province_id`,`border_id`,`company_id`,`company_name`,`mbl_no`,`declaration_type`,`icm_no`,`icm_date`,`plate_number`,`release_status`,`remark`,`docr_group`,`docr_status`,`created_at`,`updated_at`,`deleted_at`) values (18,'202403000008',8,'20240330018','2024-03-30','province_id','border_id','company_id','company_name','MBL000003','declaration_type','icm_no','2024-03-30','plate_number','release_status','remark',8,'Y','2024-03-30 23:41:13','2024-03-30 23:41:13',NULL);
insert  into `document_release`(`id`,`docr_group_code`,`docr_group_rno`,`docr_code`,`docr_date`,`province_id`,`border_id`,`company_id`,`company_name`,`mbl_no`,`declaration_type`,`icm_no`,`icm_date`,`plate_number`,`release_status`,`remark`,`docr_group`,`docr_status`,`created_at`,`updated_at`,`deleted_at`) values (19,'202403000008',8,'20240330019','2024-03-30','province_id','border_id','company_id','company_name','MBL000004','declaration_type','icm_no','2024-03-30','plate_number','release_status','remark',8,'Y','2024-03-30 23:41:13','2024-03-30 23:41:13',NULL);
insert  into `document_release`(`id`,`docr_group_code`,`docr_group_rno`,`docr_code`,`docr_date`,`province_id`,`border_id`,`company_id`,`company_name`,`mbl_no`,`declaration_type`,`icm_no`,`icm_date`,`plate_number`,`release_status`,`remark`,`docr_group`,`docr_status`,`created_at`,`updated_at`,`deleted_at`) values (20,'202403000008',8,'20240330020','2024-03-30','province_id','border_id','company_id','company_name','MBL000005','declaration_type','icm_no','2024-03-30','plate_number','release_status','remark',8,'Y','2024-03-30 23:41:13','2024-03-30 23:41:13',NULL);
insert  into `document_release`(`id`,`docr_group_code`,`docr_group_rno`,`docr_code`,`docr_date`,`province_id`,`border_id`,`company_id`,`company_name`,`mbl_no`,`declaration_type`,`icm_no`,`icm_date`,`plate_number`,`release_status`,`remark`,`docr_group`,`docr_status`,`created_at`,`updated_at`,`deleted_at`) values (21,'202403000009',9,'20240330021','2024-03-30','province_id','border_id','company_id','company_name','MBL000001','declaration_type','icm_no','2024-03-30','plate_number','release_status','remark',9,'Y','2024-03-30 23:41:14','2024-03-30 23:41:14',NULL);
insert  into `document_release`(`id`,`docr_group_code`,`docr_group_rno`,`docr_code`,`docr_date`,`province_id`,`border_id`,`company_id`,`company_name`,`mbl_no`,`declaration_type`,`icm_no`,`icm_date`,`plate_number`,`release_status`,`remark`,`docr_group`,`docr_status`,`created_at`,`updated_at`,`deleted_at`) values (22,'202403000009',9,'20240330022','2024-03-30','province_id','border_id','company_id','company_name','MBL000002','declaration_type','icm_no','2024-03-30','plate_number','release_status','remark',9,'Y','2024-03-30 23:41:14','2024-03-30 23:41:14',NULL);
insert  into `document_release`(`id`,`docr_group_code`,`docr_group_rno`,`docr_code`,`docr_date`,`province_id`,`border_id`,`company_id`,`company_name`,`mbl_no`,`declaration_type`,`icm_no`,`icm_date`,`plate_number`,`release_status`,`remark`,`docr_group`,`docr_status`,`created_at`,`updated_at`,`deleted_at`) values (23,'202403000010',10,'20240330023','2024-03-30','province_id','border_id','company_id','company_name','MBL000003','declaration_type','icm_no','2024-03-30','plate_number','release_status','remark',10,'Y','2024-03-30 23:41:14','2024-03-30 23:41:14',NULL);
insert  into `document_release`(`id`,`docr_group_code`,`docr_group_rno`,`docr_code`,`docr_date`,`province_id`,`border_id`,`company_id`,`company_name`,`mbl_no`,`declaration_type`,`icm_no`,`icm_date`,`plate_number`,`release_status`,`remark`,`docr_group`,`docr_status`,`created_at`,`updated_at`,`deleted_at`) values (24,'202403000010',10,'20240330024','2024-03-30','province_id','border_id','company_id','company_name','MBL000004','declaration_type','icm_no','2024-03-30','plate_number','release_status','remark',10,'Y','2024-03-30 23:41:14','2024-03-30 23:41:14',NULL);
insert  into `document_release`(`id`,`docr_group_code`,`docr_group_rno`,`docr_code`,`docr_date`,`province_id`,`border_id`,`company_id`,`company_name`,`mbl_no`,`declaration_type`,`icm_no`,`icm_date`,`plate_number`,`release_status`,`remark`,`docr_group`,`docr_status`,`created_at`,`updated_at`,`deleted_at`) values (25,'202403000010',10,'20240330025','2024-03-30','province_id','border_id','company_id','company_name','MBL000005','declaration_type','icm_no','2024-03-30','plate_number','release_status','remark',10,'Y','2024-03-30 23:41:14','2024-03-30 23:41:14',NULL);
insert  into `document_release`(`id`,`docr_group_code`,`docr_group_rno`,`docr_code`,`docr_date`,`province_id`,`border_id`,`company_id`,`company_name`,`mbl_no`,`declaration_type`,`icm_no`,`icm_date`,`plate_number`,`release_status`,`remark`,`docr_group`,`docr_status`,`created_at`,`updated_at`,`deleted_at`) values (26,'202403000011',11,'20240330026','2024-03-30','province_id','border_id','company_id','company_name','MBL000001','declaration_type','icm_no','2024-03-30','plate_number','release_status','remark',11,'Y','2024-03-30 23:41:14','2024-03-30 23:41:14',NULL);
insert  into `document_release`(`id`,`docr_group_code`,`docr_group_rno`,`docr_code`,`docr_date`,`province_id`,`border_id`,`company_id`,`company_name`,`mbl_no`,`declaration_type`,`icm_no`,`icm_date`,`plate_number`,`release_status`,`remark`,`docr_group`,`docr_status`,`created_at`,`updated_at`,`deleted_at`) values (27,'202403000011',11,'20240330027','2024-03-30','province_id','border_id','company_id','company_name','MBL000002','declaration_type','icm_no','2024-03-30','plate_number','release_status','remark',11,'Y','2024-03-30 23:41:14','2024-03-30 23:41:14',NULL);
insert  into `document_release`(`id`,`docr_group_code`,`docr_group_rno`,`docr_code`,`docr_date`,`province_id`,`border_id`,`company_id`,`company_name`,`mbl_no`,`declaration_type`,`icm_no`,`icm_date`,`plate_number`,`release_status`,`remark`,`docr_group`,`docr_status`,`created_at`,`updated_at`,`deleted_at`) values (28,'202403000012',12,'20240330028','2024-03-30','province_id','border_id','company_id','company_name','MBL000003','declaration_type','icm_no','2024-03-30','plate_number','release_status','remark',12,'Y','2024-03-30 23:41:14','2024-03-30 23:41:14',NULL);
insert  into `document_release`(`id`,`docr_group_code`,`docr_group_rno`,`docr_code`,`docr_date`,`province_id`,`border_id`,`company_id`,`company_name`,`mbl_no`,`declaration_type`,`icm_no`,`icm_date`,`plate_number`,`release_status`,`remark`,`docr_group`,`docr_status`,`created_at`,`updated_at`,`deleted_at`) values (29,'202403000012',12,'20240330029','2024-03-30','province_id','border_id','company_id','company_name','MBL000004','declaration_type','icm_no','2024-03-30','plate_number','release_status','remark',12,'Y','2024-03-30 23:41:14','2024-03-30 23:41:14',NULL);
insert  into `document_release`(`id`,`docr_group_code`,`docr_group_rno`,`docr_code`,`docr_date`,`province_id`,`border_id`,`company_id`,`company_name`,`mbl_no`,`declaration_type`,`icm_no`,`icm_date`,`plate_number`,`release_status`,`remark`,`docr_group`,`docr_status`,`created_at`,`updated_at`,`deleted_at`) values (30,'202403000012',12,'20240330030','2024-03-30','province_id','border_id','company_id','company_name','MBL000005','declaration_type','icm_no','2024-03-30','plate_number','release_status','remark',12,'Y','2024-03-30 23:41:15','2024-03-30 23:41:15',NULL);

/*Table structure for table `failed_jobs` */

CREATE TABLE `failed_jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `uuid` varchar(255) NOT NULL,
  `connection` text NOT NULL,
  `queue` text NOT NULL,
  `payload` longtext NOT NULL,
  `exception` longtext NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`),
  UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_general_ci;

/*Data for the table `failed_jobs` */

/*Table structure for table `job_batches` */

CREATE TABLE `job_batches` (
  `id` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `total_jobs` int(11) NOT NULL,
  `pending_jobs` int(11) NOT NULL,
  `failed_jobs` int(11) NOT NULL,
  `failed_job_ids` longtext NOT NULL,
  `options` mediumtext DEFAULT NULL,
  `cancelled_at` int(11) DEFAULT NULL,
  `created_at` int(11) NOT NULL,
  `finished_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_general_ci;

/*Data for the table `job_batches` */

/*Table structure for table `jobs` */

CREATE TABLE `jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `queue` varchar(255) NOT NULL,
  `payload` longtext NOT NULL,
  `attempts` tinyint(3) unsigned NOT NULL,
  `reserved_at` int(10) unsigned DEFAULT NULL,
  `available_at` int(10) unsigned NOT NULL,
  `created_at` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `jobs_queue_index` (`queue`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_general_ci;

/*Data for the table `jobs` */

/*Table structure for table `migrations` */

CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_general_ci;

/*Data for the table `migrations` */

insert  into `migrations`(`id`,`migration`,`batch`) values (1,'0001_01_01_000000_create_users_table',1);
insert  into `migrations`(`id`,`migration`,`batch`) values (2,'0001_01_01_000001_create_cache_table',1);
insert  into `migrations`(`id`,`migration`,`batch`) values (3,'0001_01_01_000002_create_jobs_table',1);
insert  into `migrations`(`id`,`migration`,`batch`) values (4,'2024_03_30_084752_create_personal_access_tokens_table',1);

/*Table structure for table `password_reset_tokens` */

CREATE TABLE `password_reset_tokens` (
  `email` varchar(255) NOT NULL,
  `token` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_general_ci;

/*Data for the table `password_reset_tokens` */

/*Table structure for table `personal_access_tokens` */

CREATE TABLE `personal_access_tokens` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `tokenable_type` varchar(255) NOT NULL,
  `tokenable_id` bigint(20) unsigned NOT NULL,
  `name` varchar(255) NOT NULL,
  `token` varchar(64) NOT NULL,
  `abilities` text DEFAULT NULL,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `expires_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_general_ci;

/*Data for the table `personal_access_tokens` */

insert  into `personal_access_tokens`(`id`,`tokenable_type`,`tokenable_id`,`name`,`token`,`abilities`,`last_used_at`,`expires_at`,`created_at`,`updated_at`) values (1,'App\\Models\\User',1,'visakhone sanamongkhoun','4dbd2e34f36f1580aa4d26f49a8d79c13a46bccff39a6411ae29f176956c810b','[\"*\"]',NULL,'2024-03-31 10:04:02','2024-03-30 22:04:02','2024-03-30 22:04:02');
insert  into `personal_access_tokens`(`id`,`tokenable_type`,`tokenable_id`,`name`,`token`,`abilities`,`last_used_at`,`expires_at`,`created_at`,`updated_at`) values (2,'App\\Models\\User',1,'visakhone sanamongkhoun','eb5ff677596b0c6568cac54f35f8c1a3df0f06a40b542003122b6f25d116b8af','[\"*\"]',NULL,'2024-03-31 10:04:25','2024-03-30 22:04:25','2024-03-30 22:04:25');
insert  into `personal_access_tokens`(`id`,`tokenable_type`,`tokenable_id`,`name`,`token`,`abilities`,`last_used_at`,`expires_at`,`created_at`,`updated_at`) values (3,'App\\Models\\User',1,'visakhone sanamongkhoun','8b696b37259a90b4dcbccd48a09cabc9e1e0c4db9306b576a5b6a6b7e92e362c','[\"*\"]',NULL,'2024-03-31 10:04:28','2024-03-30 22:04:28','2024-03-30 22:04:28');
insert  into `personal_access_tokens`(`id`,`tokenable_type`,`tokenable_id`,`name`,`token`,`abilities`,`last_used_at`,`expires_at`,`created_at`,`updated_at`) values (4,'App\\Models\\User',1,'visakhone sanamongkhoun','39886d2e742c4ee6f880eaa578405d58d120158121229d3c2d1995098cb7b642','[\"*\"]',NULL,'2024-03-31 10:04:41','2024-03-30 22:04:41','2024-03-30 22:04:41');
insert  into `personal_access_tokens`(`id`,`tokenable_type`,`tokenable_id`,`name`,`token`,`abilities`,`last_used_at`,`expires_at`,`created_at`,`updated_at`) values (5,'App\\Models\\User',1,'visakhone sanamongkhoun','1de11e09f3cf90c3b3f27dc6b798048bd0134a284da91d05df76f0617557faec','[\"*\"]',NULL,'2024-03-31 10:04:48','2024-03-30 22:04:48','2024-03-30 22:04:48');
insert  into `personal_access_tokens`(`id`,`tokenable_type`,`tokenable_id`,`name`,`token`,`abilities`,`last_used_at`,`expires_at`,`created_at`,`updated_at`) values (6,'App\\Models\\User',1,'visakhone sanamongkhoun','a835089086bd8b283b88d8a60ea1e055f62712805f1571a0f3f97033e24ba9dd','[\"*\"]',NULL,'2024-03-31 10:04:50','2024-03-30 22:04:50','2024-03-30 22:04:50');
insert  into `personal_access_tokens`(`id`,`tokenable_type`,`tokenable_id`,`name`,`token`,`abilities`,`last_used_at`,`expires_at`,`created_at`,`updated_at`) values (7,'App\\Models\\User',1,'visakhone sanamongkhoun','c18ae46c2766e8aa26bc7d7acfee6a3229dbb94ebcff86eac6f5222f45c5bd6e','[\"*\"]',NULL,'2024-03-31 10:04:50','2024-03-30 22:04:50','2024-03-30 22:04:50');
insert  into `personal_access_tokens`(`id`,`tokenable_type`,`tokenable_id`,`name`,`token`,`abilities`,`last_used_at`,`expires_at`,`created_at`,`updated_at`) values (8,'App\\Models\\User',1,'visakhone sanamongkhoun','3296b0b9c5a6d51ba0397be85bb87a69eb06015848c0ef9b0d7da4ad90d032e4','[\"*\"]',NULL,'2024-03-31 10:04:51','2024-03-30 22:04:51','2024-03-30 22:04:51');
insert  into `personal_access_tokens`(`id`,`tokenable_type`,`tokenable_id`,`name`,`token`,`abilities`,`last_used_at`,`expires_at`,`created_at`,`updated_at`) values (9,'App\\Models\\User',1,'visakhone sanamongkhoun','333353a37b807a72c28c5ae15dde6e5a56e50f9a08710822f4d38a751c53d832','[\"*\"]',NULL,'2024-03-31 10:04:52','2024-03-30 22:04:52','2024-03-30 22:04:52');
insert  into `personal_access_tokens`(`id`,`tokenable_type`,`tokenable_id`,`name`,`token`,`abilities`,`last_used_at`,`expires_at`,`created_at`,`updated_at`) values (10,'App\\Models\\User',1,'visakhone sanamongkhoun','f03211242bcfc6ca912d9aee209fab5dccd35029f599b2100c7fd0eaf8693b6f','[\"*\"]','2024-03-31 00:19:08','2024-03-31 10:04:53','2024-03-30 22:04:53','2024-03-31 00:19:08');
insert  into `personal_access_tokens`(`id`,`tokenable_type`,`tokenable_id`,`name`,`token`,`abilities`,`last_used_at`,`expires_at`,`created_at`,`updated_at`) values (11,'App\\Models\\User',1,'visakhone sanamongkhoun','d9b4eb9be15c35bc398d4f1ac6040807fed79124bc6eff422f8a6682297ebd5a','[\"*\"]',NULL,'2024-04-01 10:23:42','2024-03-31 22:23:42','2024-03-31 22:23:42');
insert  into `personal_access_tokens`(`id`,`tokenable_type`,`tokenable_id`,`name`,`token`,`abilities`,`last_used_at`,`expires_at`,`created_at`,`updated_at`) values (12,'App\\Models\\User',1,'visakhone sanamongkhoun','70bf2065aca3c63b9a0dae533c56ccc0137702f3ab869f5e6dee4593beca06de','[\"*\"]',NULL,'2024-04-01 10:23:57','2024-03-31 22:23:57','2024-03-31 22:23:57');
insert  into `personal_access_tokens`(`id`,`tokenable_type`,`tokenable_id`,`name`,`token`,`abilities`,`last_used_at`,`expires_at`,`created_at`,`updated_at`) values (13,'App\\Models\\User',1,'visakhone sanamongkhoun','9f00b095e47815d0cca5683f6ff02abfc7e423dfb9d3c9536cc2b7faaa32bd99','[\"*\"]',NULL,'2024-04-01 10:24:03','2024-03-31 22:24:03','2024-03-31 22:24:03');
insert  into `personal_access_tokens`(`id`,`tokenable_type`,`tokenable_id`,`name`,`token`,`abilities`,`last_used_at`,`expires_at`,`created_at`,`updated_at`) values (14,'App\\Models\\User',1,'visakhone sanamongkhoun','e3f47ee294d2d3d870e0754b063fd186ce163ce02fd8841577b9d272370523f1','[\"*\"]',NULL,'2024-04-01 10:25:26','2024-03-31 22:25:26','2024-03-31 22:25:26');
insert  into `personal_access_tokens`(`id`,`tokenable_type`,`tokenable_id`,`name`,`token`,`abilities`,`last_used_at`,`expires_at`,`created_at`,`updated_at`) values (15,'App\\Models\\User',1,'visakhone sanamongkhoun','d55272b6219f151b19c5678c4981393a749a62a032d9a3f842d68a82b658a56f','[\"*\"]',NULL,'2024-04-01 10:27:21','2024-03-31 22:27:21','2024-03-31 22:27:21');
insert  into `personal_access_tokens`(`id`,`tokenable_type`,`tokenable_id`,`name`,`token`,`abilities`,`last_used_at`,`expires_at`,`created_at`,`updated_at`) values (16,'App\\Models\\User',1,'visakhone sanamongkhoun','01d74a46224ca0241927e6e84cd090ca3a342f8f9a40df5b30f14fe94c8b7a13','[\"*\"]',NULL,'2024-04-01 10:27:36','2024-03-31 22:27:36','2024-03-31 22:27:36');
insert  into `personal_access_tokens`(`id`,`tokenable_type`,`tokenable_id`,`name`,`token`,`abilities`,`last_used_at`,`expires_at`,`created_at`,`updated_at`) values (17,'App\\Models\\User',1,'visakhone sanamongkhoun','a0ce0992f3af196844a72f22e8bde32a0e68eafc5116701971bfe7dc56048ae3','[\"*\"]',NULL,'2024-04-01 10:27:37','2024-03-31 22:27:37','2024-03-31 22:27:37');
insert  into `personal_access_tokens`(`id`,`tokenable_type`,`tokenable_id`,`name`,`token`,`abilities`,`last_used_at`,`expires_at`,`created_at`,`updated_at`) values (18,'App\\Models\\User',1,'visakhone sanamongkhoun','2da81b51c186edafa52c3dd16345c2d5065b6c18e4fa786c42a4092b07709a51','[\"*\"]',NULL,'2024-04-01 10:27:39','2024-03-31 22:27:39','2024-03-31 22:27:39');
insert  into `personal_access_tokens`(`id`,`tokenable_type`,`tokenable_id`,`name`,`token`,`abilities`,`last_used_at`,`expires_at`,`created_at`,`updated_at`) values (19,'App\\Models\\User',1,'visakhone sanamongkhoun','6e61c2fa934e11360a1ede7b7ae1b54bf6e92b916845248e9013ab1eb7a66231','[\"*\"]',NULL,'2024-04-01 10:27:56','2024-03-31 22:27:56','2024-03-31 22:27:56');
insert  into `personal_access_tokens`(`id`,`tokenable_type`,`tokenable_id`,`name`,`token`,`abilities`,`last_used_at`,`expires_at`,`created_at`,`updated_at`) values (20,'App\\Models\\User',1,'visakhone sanamongkhoun','5f062f4ded749ec31f123b12344aa33292924a9bd4f13ac753ee8ce143f6d5e0','[\"*\"]',NULL,'2024-04-01 10:27:59','2024-03-31 22:27:59','2024-03-31 22:27:59');
insert  into `personal_access_tokens`(`id`,`tokenable_type`,`tokenable_id`,`name`,`token`,`abilities`,`last_used_at`,`expires_at`,`created_at`,`updated_at`) values (21,'App\\Models\\User',1,'visakhone sanamongkhoun','1ab2458ed0932bffcbf2a099bdc207707c08ea7cee2458423fc1426a917ed9e7','[\"*\"]',NULL,'2024-04-01 10:29:52','2024-03-31 22:29:52','2024-03-31 22:29:52');
insert  into `personal_access_tokens`(`id`,`tokenable_type`,`tokenable_id`,`name`,`token`,`abilities`,`last_used_at`,`expires_at`,`created_at`,`updated_at`) values (22,'App\\Models\\User',1,'visakhone sanamongkhoun','66738bf8f5c729c9b162d0a0ac0a44e1b26321c0ff30ae36fdef6282721fcc2e','[\"*\"]',NULL,'2024-04-01 10:29:56','2024-03-31 22:29:56','2024-03-31 22:29:56');
insert  into `personal_access_tokens`(`id`,`tokenable_type`,`tokenable_id`,`name`,`token`,`abilities`,`last_used_at`,`expires_at`,`created_at`,`updated_at`) values (23,'App\\Models\\User',1,'visakhone sanamongkhoun','2f60a352fe485c554639d3025ad58e7241d0b0fd15ac26cfacc5239f043f3549','[\"*\"]',NULL,'2024-04-01 10:30:17','2024-03-31 22:30:17','2024-03-31 22:30:17');
insert  into `personal_access_tokens`(`id`,`tokenable_type`,`tokenable_id`,`name`,`token`,`abilities`,`last_used_at`,`expires_at`,`created_at`,`updated_at`) values (24,'App\\Models\\User',1,'visakhone sanamongkhoun','f181cc322fe16b99ff482542e6bb7b311d7ca4b83531d3fd84e13e879714a010','[\"*\"]',NULL,'2024-04-01 10:32:14','2024-03-31 22:32:14','2024-03-31 22:32:14');
insert  into `personal_access_tokens`(`id`,`tokenable_type`,`tokenable_id`,`name`,`token`,`abilities`,`last_used_at`,`expires_at`,`created_at`,`updated_at`) values (25,'App\\Models\\User',1,'visakhone sanamongkhoun','ac0b7b2183d9c0e1521edb06425605e9a4ffb2dda0c9579b8c0927573d6f395e','[\"*\"]',NULL,'2024-04-01 10:32:29','2024-03-31 22:32:29','2024-03-31 22:32:29');
insert  into `personal_access_tokens`(`id`,`tokenable_type`,`tokenable_id`,`name`,`token`,`abilities`,`last_used_at`,`expires_at`,`created_at`,`updated_at`) values (26,'App\\Models\\User',1,'visakhone sanamongkhoun','16fc9d9a080c703f9ef08f09c3bef8d07c13e7983c7b2835cf1baf6f42787eaf','[\"*\"]',NULL,'2024-04-01 10:32:33','2024-03-31 22:32:33','2024-03-31 22:32:33');
insert  into `personal_access_tokens`(`id`,`tokenable_type`,`tokenable_id`,`name`,`token`,`abilities`,`last_used_at`,`expires_at`,`created_at`,`updated_at`) values (27,'App\\Models\\User',1,'visakhone sanamongkhoun','c36fc85ddc11b793cbf64bea5e01f6ae2ccfbdd19ffc39bd32134db62dd18a21','[\"*\"]',NULL,'2024-04-02 09:33:02','2024-04-01 21:33:02','2024-04-01 21:33:02');
insert  into `personal_access_tokens`(`id`,`tokenable_type`,`tokenable_id`,`name`,`token`,`abilities`,`last_used_at`,`expires_at`,`created_at`,`updated_at`) values (28,'App\\Models\\User',1,'visakhone sanamongkhoun','595a79201eec8bf1bc0911f37332034e19b9701a6460f9398997178663b63ee8','[\"*\"]','2024-04-01 21:55:01','2024-04-02 09:33:45','2024-04-01 21:33:45','2024-04-01 21:55:01');
insert  into `personal_access_tokens`(`id`,`tokenable_type`,`tokenable_id`,`name`,`token`,`abilities`,`last_used_at`,`expires_at`,`created_at`,`updated_at`) values (29,'App\\Models\\User',1,'visakhone sanamongkhoun','a92b0c2962f7ccf2f73cc2bb6902617a3fc7b21be1c8aa8dd446dc11a56eb197','[\"*\"]','2024-04-01 21:57:07','2024-04-02 09:57:07','2024-04-01 21:57:07','2024-04-01 21:57:07');
insert  into `personal_access_tokens`(`id`,`tokenable_type`,`tokenable_id`,`name`,`token`,`abilities`,`last_used_at`,`expires_at`,`created_at`,`updated_at`) values (30,'App\\Models\\User',1,'visakhone sanamongkhoun','d34e4a7a27b59c50d02fe49fe2354573cb573d91934370bb1bea1e2db44c68a9','[\"*\"]','2024-04-01 23:54:22','2024-04-02 09:59:18','2024-04-01 21:59:18','2024-04-01 23:54:22');

/*Table structure for table `sessions` */

CREATE TABLE `sessions` (
  `id` varchar(255) NOT NULL,
  `user_id` bigint(20) unsigned DEFAULT NULL,
  `ip_address` varchar(45) DEFAULT NULL,
  `user_agent` text DEFAULT NULL,
  `payload` longtext NOT NULL,
  `last_activity` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `sessions_user_id_index` (`user_id`),
  KEY `sessions_last_activity_index` (`last_activity`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_general_ci;

/*Data for the table `sessions` */

insert  into `sessions`(`id`,`user_id`,`ip_address`,`user_agent`,`payload`,`last_activity`) values ('81z9CyJebn2AdJ9HRYVuDiwsQ82lU4PZenTSJe2c',NULL,'127.0.0.1','PostmanRuntime/7.37.0','YTozOntzOjY6Il90b2tlbiI7czo0MDoiaDJxMW5rVGV5cUNWU1ZwanBGQUtHN2R2YzB0dEk0a1IwbkpvRmt0QiI7czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6Mjk6Imh0dHA6Ly9sb2NhbGhvc3Q6ODU0NS9wcmV2aWV3Ijt9czo2OiJfZmxhc2giO2E6Mjp7czozOiJvbGQiO2E6MDp7fXM6MzoibmV3IjthOjA6e319fQ==',1711882076);
insert  into `sessions`(`id`,`user_id`,`ip_address`,`user_agent`,`payload`,`last_activity`) values ('G61jP2Mzd7z9ziXgZMASwp7hAYIwge2zRVKHBsuf',NULL,'127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36','YTozOntzOjY6Il90b2tlbiI7czo0MDoiaGlNc2hnWFgzazYyclpORnZkS0hJY3VVMWdBaWltOFBhbGdmM2xwaSI7czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6Mjk6Imh0dHA6Ly9sb2NhbGhvc3Q6ODU0NS9wcmV2aWV3Ijt9czo2OiJfZmxhc2giO2E6Mjp7czozOiJvbGQiO2E6MDp7fXM6MzoibmV3IjthOjA6e319fQ==',1711893081);
insert  into `sessions`(`id`,`user_id`,`ip_address`,`user_agent`,`payload`,`last_activity`) values ('nyWG0lV1gi5Q1nMtfB3oI2N4FGlPE9F7kVNf0PgC',NULL,'127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36','YTozOntzOjY6Il90b2tlbiI7czo0MDoicFZVMFBxTUJ4T3RUNHVFQTB2Z21UZWR0d0d3OGZSQ1I5ZTBSMUFLcSI7czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6Mjk6Imh0dHA6Ly9sb2NhbGhvc3Q6ODU0NS9wcmV2aWV3Ijt9czo2OiJfZmxhc2giO2E6Mjp7czozOiJvbGQiO2E6MDp7fXM6MzoibmV3IjthOjA6e319fQ==',1711882345);

/*Table structure for table `users` */

CREATE TABLE `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `fullname` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `role_id` int(11) DEFAULT NULL,
  `level_id` int(11) DEFAULT NULL,
  `province_id` varchar(255) DEFAULT NULL,
  `border_id` varchar(255) DEFAULT NULL,
  `is_active` varchar(255) DEFAULT NULL,
  `is_change_password` varchar(255) DEFAULT NULL,
  `last_login` timestamp NULL DEFAULT NULL,
  `remember_token` varchar(100) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_general_ci;

/*Data for the table `users` */

insert  into `users`(`id`,`fullname`,`email`,`email_verified_at`,`password`,`role_id`,`level_id`,`province_id`,`border_id`,`is_active`,`is_change_password`,`last_login`,`remember_token`,`created_at`,`updated_at`,`deleted_at`) values (1,'visakhone sanamongkhoun','visakhone01@gmail.com',NULL,'$2y$12$5Wknnt7WYHJj7H5xQwrSQueW35whhhdnUVOu9DCYa.tzX2QSkd7lq',1,1,'01','1','Y','N',NULL,NULL,'2024-03-30 22:01:57','2024-03-30 22:01:57',NULL);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
