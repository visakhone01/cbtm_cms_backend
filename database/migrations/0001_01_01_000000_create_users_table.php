<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Eloquent\SoftDeletes;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('fullname');
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->integer('role_id')->nullable();
            $table->integer('level_id')->nullable();
            $table->string('province_id')->nullable();
            $table->string('border_id')->nullable();
            $table->string('is_active')->nullable();
            $table->string('is_change_password')->nullable();
            $table->timestamp('last_login')->nullable();
            $table->rememberToken();
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('password_reset_tokens', function (Blueprint $table) {
            $table->string('email')->primary();
            $table->string('token');
            $table->timestamp('created_at')->nullable();
        });

        Schema::create('sessions', function (Blueprint $table) {
            $table->string('id')->primary();
            $table->foreignId('user_id')->nullable()->index();
            $table->string('ip_address', 45)->nullable();
            $table->text('user_agent')->nullable();
            $table->longText('payload');
            $table->integer('last_activity')->index();
        });

        Schema::create('auth_permision_role', function (Blueprint $table) {
            $table->id();
            $table->string('permision_role_code')->nullable();
            $table->string('permision_role_name')->nullable();
            $table->string('permision_role_desc')->nullable();
            $table->string('permision_role_status')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('auth_permision', function (Blueprint $table) {
            $table->id();
            $table->string('permistion_code')->nullable();
            $table->integer('role_id')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('auth_role_users', function (Blueprint $table) {
            $table->id();
            $table->integer('user_id')->nullable();
            $table->integer('role_id')->nullable();
            $table->string('role_users_status')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('auth_role', function (Blueprint $table) {
            $table->id();
            $table->string('role_name')->nullable();
            $table->string('role_desc')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('auth_menu', function (Blueprint $table) {
            $table->id();
            $table->string('menu_code')->nullable();
            $table->string('menu_name')->nullable();
            $table->string('menu_url')->nullable();
            $table->string('menu_icon')->nullable();
            $table->string('menu_image')->nullable();
            $table->integer('menu_parent')->nullable();
            $table->integer('menu_level')->nullable();
            $table->integer('menu_group')->nullable();
            $table->integer('menu_order')->nullable();
            $table->string('menu_status')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
        
        
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('users');
        Schema::dropIfExists('password_reset_tokens');
        Schema::dropIfExists('sessions');
        Schema::dropIfExists('auth_permision_role');
        Schema::dropIfExists('auth_permision');
        Schema::dropIfExists('auth_role_users');
        Schema::dropIfExists('auth_role');
        Schema::dropIfExists('auth_menu');
    }
};
