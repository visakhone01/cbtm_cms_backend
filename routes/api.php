<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\DocumentController;

Route::get('/user', function (Request $request) {
    return $request->user();
})->middleware('auth:sanctum');


Route::group(['prefix' => 'auth', 'middleware' => []], function () {
    Route::post('/login', [AuthController::class, 'login']);
    Route::post('/users', [AuthController::class, 'addUsers']);
});

Route::group(['prefix' => 'auth', 'middleware' => ['auth:sanctum']], function () {
    Route::get('/menu', [AuthController::class, 'listMenu']);
});

Route::group(['prefix' => 'document', 'middleware' => ['auth:sanctum']], function () {
    Route::get('/release', [DocumentController::class, 'listDocumentRelease']);
    Route::post('/release', [DocumentController::class, 'addDocumentRelease']);
});

