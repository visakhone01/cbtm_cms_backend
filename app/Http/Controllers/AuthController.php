<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Menu;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;

use Exception;

class AuthController extends Controller{

    public function login(Request $request){
        try {
            $user = User::where("users.is_active", "=", "Y")
                        ->where("users.email", "=", $request->username)
                        ->leftJoin("auth_role_users", "users.role_id", "=", "auth_role_users.user_id")
                        ->leftJoin("auth_role", "auth_role_users.role_id", "=", "auth_role.id")
                        ->select("users.*", "auth_role.role_name")
                        ->first();

            if(!isset($user->email)){
                return response()->json([
                    "code" => 202,
                    "success" => false,
                    'message' => 'ບໍ່ມີຂໍ້ມູນຜູ້ໃຊ້ລະບົບ ' . $request->username
                ],402);
            }

            if(!Hash::check($request->password, $user->password)){
                return response()->json([
                    "code" => 200,
                    "success" => false,
                    'message' => 'ຊື່ຜູ້ໃຊ້ ຫລື ລະຫັດຜ່ານບໍ່ຖືກຕ້ອງ'
                ],402);
            }
            
            $token = $user->createToken($user->fullname, ['*'], now()->addHour(12))->plainTextToken;
            // $token = "";

            $res = array(
                "code" => 200,
                "success" => true,
                "message" => "Login Success",
                "data" => [
                    "token" => $token,
                    "fullname" => $user->fullname,
                    "email" => $user->email,
                    "role" => $user->role_name,
                ]
            );
            return response()->json($res, 200);
        } catch (Exception $e) {
            $res = array(
                "code" => 202,
                "success" => false,
                "message" => $e->getMessage(),
            );
            return response()->json($res, 500);
        }
    }

    public function addUsers(Request $request){
        try {
            $check = User::where("email", "=", $request->email)->first();                                                                                      

            if(isset($check->email)){
                $res = array(
                    "code" => 201,
                    "success" => false,
                    "message" => "ບໍ່ມີຂໍ້ມູນ " . $request->email . " ໃນລະບົບ",
                );
                return response()->json($res, 402);
            }

            $user = new User([
                "fullname" => $request->fullname,
                "email" => $request->email,
                "password" => bcrypt($request->password),
                "role_id" => $request->role_id,
                "level_id" => $request->level_id,
                "province_id" => $request->province_id,
                "border_id" => $request->border_id,
                "is_active" => "Y",
                "is_change_password" => "N",
            ]);

            if($user->save()){
                $res = array(
                    "code" => 200,
                    "success" => true,
                    "message" => "ບັນທຶກຂໍ້ມູນສຳເລັດ",
                );
            }else{
                $res = array(
                    "code" => 202,
                    "success" => false,
                    "message" => "ເກີດຂໍ້ຜິດພາດ...ກະລຸນາແຈ້ງຜູ້ພັດທະນາລະບົບ",
                );
            }

            return response()->json($res, 200);
        } catch (Exception $e) {
            $res = array(
                "code" => 202,
                "success" => false,
                "message" => $e->getMessage(),
            );
            return response()->json($res, 500);
        }
    }

    public function listMenu(Request $request){
        try {
            
            $menu = Menu::where("menu_status", "=", "Y")->orderby("menu_order","asc")->get();

            $final_result = array();

            foreach ($menu as $i => $rows) {
                if($rows["menu_level"] == "0"){
                    $obj = array(
                        "menu_code" => $rows["menu_code"],
                        "menu_name" => $rows["menu_name"],
                        "menu_url" => $rows["menu_url"],
                        "menu_icon" => $rows["menu_icon"],
                        "menu_image" => $rows["menu_image"],
                        "menu_sub" => array(),
                    );
                    array_push($final_result, $obj);
                }elseif($rows["menu_level"] == "1"){
                    $menu_sub = array();
                    foreach ($menu as $key => $value) {
                        if($value["menu_level"] == "2" && $value["menu_group"] == $rows["menu_group"]){
                            $obj_sub = array(
                                "menu_code" => $value["menu_code"],
                                "menu_name" => $value["menu_name"],
                                "menu_url" => $value["menu_url"],
                                "menu_icon" => $value["menu_icon"],
                                "menu_image" => $value["menu_image"],
                            );
                            array_push($menu_sub, $obj_sub);

                        }
                    }
                    $obj = array(
                        "menu_code" => $rows["menu_code"],
                        "menu_name" => $rows["menu_name"],
                        "menu_url" => $rows["menu_url"],
                        "menu_icon" => $rows["menu_icon"],
                        "menu_image" => $rows["menu_image"],
                        "menu_sub" => $menu_sub,
                    );
                    array_push($final_result, $obj);
                }
            }

            $res = array(
                "code" => 200,
                "success" => true,
                "message" => "ດຶງຂໍ້ມູນສຳເລັດ",
                "data" => $final_result,
            );
            return response()->json($res, 200);
        } catch (Exception $e) {
            $res = array(
                "code" => 202,
                "success" => false,
                "message" => $e->getMessage(),
            );
            return response()->json($res, 500);
        }
    }



}
