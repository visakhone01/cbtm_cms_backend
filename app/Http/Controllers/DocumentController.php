<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Document_release;
use Barryvdh\DomPDF\Facade\Pdf;
// use Spatie\LaravelPdf\Facades\Pdf;

use Exception;

class DocumentController extends Controller
{
    public function addDocumentRelease(Request $request){
        try {

            $data = [
                [
                    "company_id"=> "company_id",
                    "company_name"=> "company_name",
                    "province_id"=> "province_id",
                    "border_id"=> "border_id",
                    "list_vehicle" => [
                        [
                            "mbl_no" => "MBL000001",
                            "declaration_type"=> "declaration_type",
                            "icm_no"=> "icm_no",
                            "release_no" => "release_no",
                            "icm_date"=> "2024-03-30",
                            "plate_number"=> "plate_number",
                            "release_status"=> "release_status",
                            "remark"=> "remark"
                        ],
                        [
                            "mbl_no" => "MBL000002",
                            "declaration_type"=> "declaration_type",
                            "icm_no"=> "icm_no",
                            "release_no" => "release_no",
                            "icm_date"=> "2024-03-30",
                            "plate_number"=> "plate_number",
                            "release_status"=> "release_status",
                            "remark"=> "remark"
                        ]
                    ]
                ],
                [
                    "company_id"=> "company_id",
                    "company_name"=> "company_name",
                    "province_id"=> "province_id",
                    "border_id"=> "border_id",
                    "list_vehicle" => [
                        [
                            "mbl_no" => "MBL000003",
                            "declaration_type"=> "declaration_type",
                            "icm_no"=> "icm_no",
                            "release_no" => "release_no",
                            "icm_date"=> "2024-03-30",
                            "plate_number"=> "plate_number",
                            "release_status"=> "release_status",
                            "remark"=> "remark"
                        ],
                        [
                            "mbl_no" => "MBL000004",
                            "declaration_type"=> "declaration_type",
                            "icm_no"=> "icm_no",
                            "release_no" => "release_no",
                            "icm_date"=> "2024-03-30",
                            "plate_number"=> "plate_number",
                            "release_status"=> "release_status",
                            "remark"=> "remark"
                        ],
                        [
                            "mbl_no" => "MBL000005",
                            "declaration_type"=> "declaration_type",
                            "icm_no"=> "icm_no",
                            "release_no" => "release_no",
                            "icm_date"=> "2024-03-30",
                            "plate_number"=> "plate_number",
                            "release_status"=> "release_status",
                            "remark"=> "remark"
                        ]
                    ]
                ]
            ];

            foreach ($data as $key => $value) {
                $group = Document_release::orderby("docr_group", "desc")->first();
                if(isset($group['docr_group'])){
                    $last_group = intval($group['docr_group']) + 1;
                }else{
                    $last_group = intval(1);
                }

                $group2 = Document_release::whereYear("docr_date", "=", date("Y"))
                        ->whereMonth("docr_date", "=", date("m"))
                        ->where("border_id", "=", $value["border_id"])
                        ->orderby("docr_group_rno", "desc")
                        ->first();

                if(isset($group2['docr_group_rno'])){
                    $docr_group_rno = intval($group2['docr_group_rno']) + 1;
                    $last_doc_group = date('Ym') . str_pad($docr_group_rno, 6, "0", STR_PAD_LEFT);
                }else{
                    $docr_group_rno = intval(1);
                    $last_doc_group = date('Ym') . str_pad($docr_group_rno, 6, "0", STR_PAD_LEFT);
                }
                foreach ($value["list_vehicle"] as $i => $rows) {
                    $document_release = new Document_release([
                        "docr_group_code" => $last_doc_group,
                        "docr_group_rno" => $docr_group_rno,
                        "docr_date" => now(),
                        "company_id" => $value["company_id"],
                        "company_name" => $value["company_name"],
                        "province_id" => $value["province_id"],
                        "border_id" => $value["border_id"],
                        "mbl_no" => $rows["mbl_no"],
                        "declaration_type" => $rows["declaration_type"],
                        "icm_no" => $rows["icm_no"],
                        "icm_date" => $rows["icm_date"],
                        "release_no" => $rows["release_no"],
                        "plate_number" => $rows["plate_number"],
                        "release_status" => $rows["release_status"],
                        "remark" => $rows["remark"],
                        "docr_group" => $last_group,
                        "docr_status" => "Y",
                    ]);
                    $document_release->save();
                }
                
            }

            $res = array(
                "code" => 200,
                "success" => true,
                "message" => "ບັນທຶກຂໍ້ມູນສຳເລັດ",
                "data" => $data,
            );

            return response()->json($res, 200);
        } catch (Exception $e) {
            $res = array(
                "code" => 202,
                "success" => false,
                "message" => $e->getMessage(),
            );
            return response()->json($res, 500);
        }
    }

    public function listDocumentRelease(Request $request){
        try {
            if($request->id == ""){
                $document = Document_release::orderby("docr_group_code", "desc")->get();
            }else{
                $document = Document_release::where("docr_group_code", "=", $request->id)->orderby("docr_group_code", "desc")->get();
            }
            
            $group_doc = [];
            $group_doc_data = [];

            foreach ($document as $key => $value) {
                if(!in_array($value["docr_group_code"], $group_doc)){
                    array_push($group_doc, $value["docr_group_code"]);
                    array_push($group_doc_data, $value);
                }
            }

            $final_result = array();
            
            foreach ($group_doc_data as $index => $items) {
                $sub_data = array();
                foreach ($document as $i => $rows) {
                    if($items["docr_group_code"] == $rows["docr_group_code"]){
                        $obj = array(
                            "mbl_no" => $rows["mbl_no"],
                            "declaration_type" => $rows["declaration_type"],
                            "icm_no" => $rows["icm_no"],
                            "icm_date" => $rows["icm_date"],
                            "release_no" => $rows["release_no"],
                            "plate_number" => $rows["plate_number"],
                            "release_status" => $rows["release_status"],
                            "remark" => $rows["remark"],
                        );

                        array_push($sub_data, $obj);
                    }
                }
                array_push($final_result, array(
                        "doc_group" => $items["docr_group_code"],
                        "doc_code" => $items["docr_code"],
                        "doc_date" => $items["docr_date"],
                        "province_id" => $items["province_id"],
                        "border_id" => $items["border_id"],
                        "company_id" => $items["company_id"],
                        "company_name" => $items["company_name"],
                        "list_vehicle" => $sub_data
                    )
                );
            }

            $res = array(
                "code" => 200,
                "success" => true,
                "message" => "ດຶງຂໍ້ມູນສຳເລັດ",
                "data" => $final_result,
            );

            return response()->json($res, 200);
        } catch (Exception $e) {
            $res = array(
                "code" => 202,
                "success" => false,
                "message" => $e->getMessage(),
            );
            return response()->json($res, 500);
        }
    }

    public function previewDocumentRelease(){

        $pdf = Pdf::loadView('test');
        $pdf->setOption(['dpi' => 125, 'defaultFont' => 'phetsarath ot'])->setPaper('a4', 'portrait');
        // return $pdf->stream();

        return view('test');

    }
}
