<?php

namespace App\Models;

use Haruncpi\LaravelIdGenerator\IdGenerator;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Document_release extends Model
{
    use HasFactory;

    protected $table = "document_release";

    protected $fillable = [
        'docr_group_code',
        'docr_group_rno',
        'docr_code',
        'docr_date',
        'company_id',
        'company_name',
        'mbl_no',
        'declaration_type',
        'province_id',
        'border_id',
        'icm_no',
        'icm_date',
        'release_no',
        'plate_number',
        'release_status',
        'remark',
        'docr_group',
        'docr_status',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $hidden = [
        'docr_group_rno',
        // 'docr_group',
        'docr_status',
        'created_at',
        'updated_at',
        'deleted_at',
    ]; 

    public static function boot()
    {
        parent::boot();

        self::creating(function ($model) {

            // IdGenerator is install `composer require haruncpi/laravel-id-generator`
            $model->docr_code = IdGenerator::generate([
                'table' => 'document_release',
                'field' => 'docr_code',
                'length' => 11,
                'prefix' => date('Ymd'),
                'reset_on_prefix_change' => true
            ]);
        });
    }


    
}
