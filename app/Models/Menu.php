<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    use HasFactory;

    protected $table = "auth_menu";

    protected $fillable = [
        'menu_code',
        'menu_name',
        'menu_url',
        'menu_icon',
        'menu_image',
        'menu_parent',
        'menu_level',
        'menu_group',
        'menu_order',
        'menu_status',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at',
    ]; 
}
